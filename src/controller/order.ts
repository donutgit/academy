import { Product } from '../model/product';
import MemoryProductService from '../service/order';

class MemoryOrderController {
    createProduct(data: Product) {
        MemoryProductService.saveProduct(data);
    }
}

export default new MemoryOrderController();
