import { Product } from 'src/model/product';

export enum ProductCategory {
    MATERIAL,
    INTANGIBLE,
}

export type Order = Array<Product>


