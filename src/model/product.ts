import * as uuid from 'uuid/v1';

export class Product {
    public id?: string;
    public name: string;
    public category: any;
    public color: string;
    public price: number;

    constructor(data: Product) {
        this.id = uuid();
        this.name = data.name;
        this.category = data.category;
        this.color = data.color;
        this.price = data.price;
    }

}
