import { Product } from '../model/product';

export interface OrderRepository {
    saveProduct(data: Product): Product;

    findProduct(id: string): Product;

    deleteProduct(id: string): void;
}

export class MemoryOrderRepository implements OrderRepository {
    private memory: Array<Product>;

    constructor() {
        this.memory = [];
    }

    saveProduct(product: Product): Product {
        this.memory.push(product);
        return product;
    }

    deleteProduct(id: string): void {
        this.memory.filter(p => p.id !== id);
    }

    findProduct(id: string): Product {
        return this.memory.find(i => i.id === id);
    }

}

export default new MemoryOrderRepository();
