import MemoryOrderRepository from '../repository/order';
import { Product } from '../model/product';

interface ProductService {
    saveProduct(product: Product): Product;
}

class MemoryProductService implements ProductService {
    private repository: MemoryOrderRepository;

    constructor(props: MemoryOrderRepository) {
        this.repository = props.MemoryOrderRepository;
    }

    saveProduct(data: Product): Product {
        const newProduct = new Product(data);
        return this.repository.MemoryOrderRepository.saveProduct(newProduct);
    }

}

export default new MemoryProductService(MemoryOrderRepository);
